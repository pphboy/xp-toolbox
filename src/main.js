import Vue from 'vue'
import App from './App.vue'
// 饿了么UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import VueRouter from 'vue-router'
//引入HOME文件
import Home from './components/Home'
import FollowTyping from './components/childView/FollowTyping';
// HEADER


const routes=[
    {path:'/home',component:Home},
    {path:'*',component:Home},
    {path:'/typing',component:FollowTyping}
] //默认首页

const router = new VueRouter(
    {routes}  
)

Vue.use(VueRouter)
Vue.use(ElementUI);


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
